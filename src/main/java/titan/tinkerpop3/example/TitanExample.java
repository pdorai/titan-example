package titan.tinkerpop3.example;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.hadoop.hive.ql.lib.DefaultGraphWalker;
import org.apache.hadoop.hive.ql.lib.DefaultRuleDispatcher;
import org.apache.hadoop.hive.ql.lib.Dispatcher;
import org.apache.hadoop.hive.ql.lib.GraphWalker;
import org.apache.hadoop.hive.ql.lib.Node;
import org.apache.hadoop.hive.ql.lib.NodeProcessor;
import org.apache.hadoop.hive.ql.lib.NodeProcessorCtx;
import org.apache.hadoop.hive.ql.lib.Rule;
import org.apache.hadoop.hive.ql.parse.ASTNode;
import org.apache.hadoop.hive.ql.parse.BaseSemanticAnalyzer;
import org.apache.hadoop.hive.ql.parse.HiveParser;
import org.apache.hadoop.hive.ql.parse.ParseDriver;
import org.apache.hadoop.hive.ql.parse.ParseException;
import org.apache.hadoop.hive.ql.parse.SemanticException;
import org.apache.hadoop.hive.ql.tools.LineageInfo;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Vertex;
//import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.*;
import org.apache.tinkerpop.gremlin.util.iterator.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanEdge;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.schema.Mapping;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.thinkaurelius.titan.core.util.TitanCleanup;

public class TitanExample extends LineageInfo implements Serializable {

	private static final Logger Logger = LoggerFactory.getLogger(TitanExample.class);
	private static final long serialVersionUID = -2764517278034419985L;

	private static TitanGraph graph;
	private static String PROPS_PATH = "titan-berkeleyje-es.properties";
	static HashMap<String, Object> lineageMap = null;

	/**
	 * parses given query and gets the lineage info.
	 *
	 * @param query
	 * @throws ParseException
	 */
	public void getLineageInfo(String query) throws ParseException, SemanticException {
		/*
		 * Get the AST tree
		 */
		ParseDriver pd = new ParseDriver();
		ASTNode tree = pd.parse(query);
		System.out
				.println("*****************************************start*****************************************\n\n");
		while ((tree.getToken() == null) && (tree.getChildCount() > 0)) {
			System.out.println("********************* node dump= " + tree.dump());
			tree = (ASTNode) tree.getChild(0);
			System.out.println(
					"*****************************************end*****************************************\n\n");
		}

		// create a walker which walks the tree in a DFS manner while
		// maintaining
		// the operator stack. The dispatcher
		// generates the plan from the operator tree
		Map<Rule, NodeProcessor> rules = new LinkedHashMap<Rule, NodeProcessor>();

		// The dispatcher fires the processor corresponding to the closest
		// matching
		// rule and passes the context along
		Dispatcher disp = new DefaultRuleDispatcher(this, rules, null);
		GraphWalker ogw = new DefaultGraphWalker(disp);

		// Create a list of topop nodes
		ArrayList<Node> topNodes = new ArrayList<Node>();
		topNodes.add(tree);
		ogw.startWalking(topNodes, null);
	}

	/**
	 * Implements the process method for the NodeProcessor interface.
	 */
	@SuppressWarnings("unchecked")
	public Object process(Node nd, Stack<Node> stack, NodeProcessorCtx procCtx, Object... nodeOutputs)
			throws SemanticException {
		ASTNode pt = (ASTNode) nd;
		int count = 0;
		TitanVertex src = null;
		TitanVertex dest = null;
		// System.out.println("***************token type= " +
		// pt.getToken().getText());
		switch (pt.getToken().getType()) {
		case HiveParser.TOK_CREATEDATABASE:
			// sample = "create database customer"
			// new database
			String database_new = BaseSemanticAnalyzer.getUnescapedName((ASTNode) pt.getChild(0));
			lineageMap.put("srcName", "TestUser");
			lineageMap.put("srcType", "user");
			lineageMap.put("destName", database_new);
			lineageMap.put("destType", "database");
			lineageMap.put("action", "created");
			break;
		case HiveParser.TOK_CREATETABLE:
			// sample = "create table branch (branch_name
			// varchar(25),branch_city varchar(25),assets varchar(25))"
			// new table
			String table_new = BaseSemanticAnalyzer.getUnescapedName((ASTNode) pt.getChild(0));
			lineageMap.put("srcName", "TestUser");
			lineageMap.put("srcType", "user");
			lineageMap.put("destName", table_new);
			lineageMap.put("destType", "table");
			lineageMap.put("action", "created");
			break;
		// case HiveParser.TOK_CREATEVIEW:
		// // sample = "create view view_1 as select b.*,s.credit_card_number
		// // from borrower b, sales s where b.customer_name = s.customer_name"
		// // new view
		// String view_new = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// entityId = EntityInfo.genExtId(view_new, EntityInfo.ENTITY_TYPES[2]);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(view_new, EntityInfo.ENTITY_TYPES[2]);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// lineageMap.put("action", "Create");
		// break;
		// case HiveParser.TOK_LOAD:
		// // Sample = "load data inpath '/user/cloudera/thousand_strings.txt'
		// // into table sample_table"
		// // load_from_file path
		// String load_from_file =
		// BaseSemanticAnalyzer.getUnescapedName((ASTNode) pt.getChild(0));
		// String src_type = EntityInfo.ENTITY_TYPES[3];
		// boolean isSet = false;
		// if (load_from_file.startsWith("file:")) {
		// src_type = EntityInfo.ENTITY_TYPES[4];
		// isSet = true;
		// }
		// if (!isSet) {
		// ArrayList<Node> childList = pt.getChildren();
		// if (childList != null) {
		// Iterator<Node> iter = childList.iterator();
		// while (iter.hasNext()) {
		// ASTNode childNode = (ASTNode) iter.next();
		// String token = childNode.getToken().getText();
		// if (token.equalsIgnoreCase("local")) {
		// src_type = EntityInfo.ENTITY_TYPES[4];
		// }
		// }
		// }
		// }
		// entityId = EntityInfo.genExtId(load_from_file, src_type);
		// EntityInfo srcEntity = entityMap.get(entityId);
		// if (srcEntity == null) {
		// srcEntity = new EntityInfo(load_from_file, src_type);
		// entityMap.put(entityId, srcEntity);
		// }
		// lineageMap.put("src_entity", entityId);
		// lineageMap.put("action", "Load");
		// break;
		// case HiveParser.TOK_DROPDATABASE:
		// // sample = "DROP database IF EXISTS partition_test"
		// // drop db
		// String drop_db = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// entityId = EntityInfo.genExtId(drop_db, EntityInfo.ENTITY_TYPES[0]);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(drop_db, EntityInfo.ENTITY_TYPES[0]);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// lineageMap.put("action", "Drop");
		// break;
		// case HiveParser.TOK_DROPTABLE:
		// // sample = "DROP TABLE pokes"
		// // drop table
		// String drop_table = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// entityId = EntityInfo.genExtId(drop_table,
		// EntityInfo.ENTITY_TYPES[1]);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(drop_table, EntityInfo.ENTITY_TYPES[1]);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// lineageMap.put("action", "Drop");
		// break;
		// case HiveParser.TOK_INSERT:
		// // sample = "INSERT OVERWRITE TABLE tabB SELECT a.Age FROM TableA a
		// // WHERE a.Age >= 18"
		// if (lineageMap.get("action") == null)
		// lineageMap.put("action", "Insert");
		// break;
		// case HiveParser.TOK_MSCK:
		// // sample = "MSCK REPAIR TABLE partition_test"
		// // msck table
		// String msktable = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(1));
		// entityId = EntityInfo.genExtId(msktable, EntityInfo.ENTITY_TYPES[1]);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(msktable, EntityInfo.ENTITY_TYPES[1]);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// lineageMap.put("action", "MSCK");
		// break;
		// case HiveParser.TOK_TAB:
		// // sample = "load data inpath '/user/cloudera/thousand_strings.txt'
		// // into table sample_table"
		// String table_name = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// entityId = EntityInfo.genExtId(table_name,
		// EntityInfo.ENTITY_TYPES[1]);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(table_name, EntityInfo.ENTITY_TYPES[1]);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// break;
		case HiveParser.TOK_TABREF:
		case HiveParser.TOK_LIKETABLE:
			// sample = "create table users_new as select user_id, user_name
			// from users where users.user_id != 0"
			// sample = "create table tableB like tableA"
			// gets parents/depends entities
			ASTNode tabTree = (ASTNode) pt.getChild(0);
			if (tabTree != null) {
				String from_table = (tabTree.getChildCount() == 1)
						? BaseSemanticAnalyzer.getUnescapedName((ASTNode) tabTree.getChild(0))
						: BaseSemanticAnalyzer.getUnescapedName((ASTNode) tabTree.getChild(1));
				// : BaseSemanticAnalyzer.getUnescapedName((ASTNode)
				// tabTree.getChild(0)) + "."
				// + tabTree.getChild(1);
				lineageMap.put("srcName", null);
				lineageMap.put("srcType", "table");
				String ref = "";
				if (pt.getChildCount() > 1) {
					ref = BaseSemanticAnalyzer.getUnescapedName((ASTNode) pt.getChild(1));
				}
				String parents = (String) lineageMap.get("depends");
				from_table = from_table + ":" + ref;
				if (parents == null) {
					parents = from_table;
				} else {
					parents = parents + "," + from_table;
				}
				lineageMap.put("depends", parents);
				Logger.info("****************got parents= " + parents);
			}
			break;
		// case HiveParser.TOK_TABLELOCATION:
		// case HiveParser.TOK_DATABASELOCATION:
		// // sample = "create external table hive_from_hdfs_tab (id int,
		// // myfields string) location '/my/location/in/hdfs'"
		// // sample = "create database if not exists hive_from_hdfs_db
		// // location '/my/location/in/hdfs'"
		// String hdfsLocation = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// entityId = EntityInfo.genExtId(hdfsLocation,
		// EntityInfo.ENTITY_TYPES[3]);
		// srcEntity = entityMap.get(entityId);
		// if (srcEntity == null) {
		// srcEntity = new EntityInfo(hdfsLocation, EntityInfo.ENTITY_TYPES[3]);
		// entityMap.put(entityId, srcEntity);
		// }
		// lineageMap.put("src_entity", entityId);
		// break;
		// case HiveParser.TOK_DIR:
		// // sample = ""INSERT OVERWRITE DIRECTORY '/tmp/hdfs_out' SELECT a.*
		// // FROM invites a WHERE a.ds='2008-08-15'""
		// hdfsLocation = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// if (hdfsLocation.equalsIgnoreCase("TOK_TMP_FILE")) {
		// // select query
		// lineageMap.put("action", "Select");
		// break;
		// }
		// isSet = false;
		// String dest_type = EntityInfo.ENTITY_TYPES[3];
		// if (hdfsLocation.startsWith("file:")) {
		// dest_type = EntityInfo.ENTITY_TYPES[4];
		// isSet = true;
		// }
		// if (!isSet) {
		// ArrayList<Node> childList = pt.getChildren();
		// if (childList != null) {
		// Iterator<Node> iter = childList.iterator();
		// while (iter.hasNext()) {
		// ASTNode childNode = (ASTNode) iter.next();
		// String token = childNode.getToken().getText();
		// if (token.equalsIgnoreCase("local")) {
		// dest_type = EntityInfo.ENTITY_TYPES[4];
		// }
		// }
		// }
		// }
		// entityId = EntityInfo.genExtId(hdfsLocation, dest_type);
		// destEntity = entityMap.get(entityId);
		// if (destEntity == null) {
		// destEntity = new EntityInfo(hdfsLocation, dest_type);
		// entityMap.put(entityId, destEntity);
		// }
		// lineageMap.put("dest_entity", entityId);
		// break;
		// case HiveParser.TOK_FUNCTION:
		// // udf function
		// String udfFunction = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// pt.getChild(0));
		// ArrayList<UDFInfo> udfList = (ArrayList<UDFInfo>)
		// lineageMap.get("udfList");
		// if (udfList == null) {
		// udfList = new ArrayList<UDFInfo>();
		// }
		// ArrayList<ColumnInfo> colList = new ArrayList<ColumnInfo>();
		// if (pt.getChildCount() > 1) {
		// for (int i = 1; i < pt.getChildCount(); i++) {
		// ASTNode cNode = (ASTNode) pt.getChild(i);
		// if (cNode.getToken().getText().equalsIgnoreCase("TOK_TABLE_OR_COL"))
		// {
		// String colName = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// cNode.getChild(0));
		// ColumnInfo col = new ColumnInfo();
		// col.getData().put("name", colName);
		// colList.add(col);
		// } else if (cNode.getToken().getText().equalsIgnoreCase(".")) {
		// for (int ci = 0; ci < cNode.getChildCount(); ci++) {
		// ASTNode tNode = (ASTNode) cNode.getChild(ci);
		// if (tNode.getToken().getText().equalsIgnoreCase("TOK_TABLE_OR_COL"))
		// {
		// ColumnInfo col = new ColumnInfo();
		// // column name
		// String colName = BaseSemanticAnalyzer
		// .getUnescapedName((ASTNode) cNode.getChild(ci + 1));
		// col.getData().put("name", colName);
		// // table reference
		// String tableRef = BaseSemanticAnalyzer.getUnescapedName((ASTNode)
		// tNode.getChild(0));
		// col.getData().put("table_ref", tableRef);
		// colList.add(col);
		// }
		// }
		// }
		// }
		// }
		// UDFInfo udf = new UDFInfo();
		// udf.getData().put("func_name", udfFunction);
		// udf.getData().put("col_list", colList);
		// udfList.add(udf);
		// lineageMap.put("udfList", udfList);
		// break;
		// default:
		}
		return null;
	}

	@SuppressWarnings({ "serial", "unchecked" })
	public static void main(String[] args) {
		System.out.println("Start Hive Query Parsing...!");
		try {
			PropertiesConfiguration conf = new PropertiesConfiguration(PROPS_PATH);
			// Configuration conf = new BaseConfiguration();
			// TitanGraph graph = TitanFactory.open("berkeleyje:" + dataDir);
			// conf.setProperty("storage.backend", "berkeleyje");
			// conf.setProperty("storage.directory", "./data");
			graph = TitanFactory.open(conf);
			if (!graph.isClosed()) {
				// clear the graph
				graph.close();
			}
			TitanCleanup.clear(graph);

			// recreate the graph
			graph = TitanFactory.open(conf);

			// Create an index
			TitanManagement mgmt = graph.openManagement();
			PropertyKey name = mgmt.makePropertyKey("name").dataType(String.class).make();
			PropertyKey type = mgmt.makePropertyKey("type").dataType(String.class).make();
			mgmt.buildIndex("byNameType", TitanVertex.class).addKey(name).addKey(type).buildCompositeIndex();
			mgmt.buildIndex("byNameAndType", TitanVertex.class).addKey(name, Mapping.STRING.asParameter())
					.addKey(type, Mapping.STRING.asParameter()).buildMixedIndex("search");
			mgmt.commit();

			// Block until the SchemaStatus transitions from INSTALLED to
			// REGISTERED
			// ManagementSystem.awaitGraphIndexStatus(graph,
			// "byNameType").status(SchemaStatus.REGISTERED).call();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!graph.tx().isOpen()) {
			graph.tx().open();
		}
		graph.tx().rollback(); // Never create new indexes while a transaction
		// is active
		final LineageInfo lep = new TitanExample();
		lineageMap = new HashMap<String, Object>();
		String[] queryList = { "create database customer"
				, "create database sales"
				,"create table users (foo1 INT, bar1 STRING)"
						,"create table invites1 (foo1 INT, bar1 STRING)"
						, "create table pokes1 (foo2 INT, bar2 STRING)"
						, "create table pokes as select foo2, bar2 from pokes1 where foo2 != 0"
						, "create table invites as select invites.*, pokes.foo2, pokes.bar2 from invites1 inner join pokes on pokes.foo2 == invites1.foo1"
						, "create table user_pokes as select user.*, pokes.foo2, pokes.bar2 from users inner join pokes on pokes.foo2 == users.foo1"};
		for (int i = 0; i < queryList.length; i++) {
			String query = queryList[i];
			Logger.info("*****************got query= " + query);
			try {
				lineageMap.clear();
				lep.getLineageInfo(query);
				int count = 0;
				lineageMap.put("user", "TestUser");
				lineageMap.put("ip", "127.0.0.1");
				lineageMap.put("host", "hdppii2");
				lineageMap.put("evtTime", "2016-12-29T00:00Z");
				lineageMap.put("permission", "rwx--r--r");
				lineageMap.put("result", "0");
				String srcName = (String) lineageMap.get("srcName");
				String srcType = (String) lineageMap.get("srcType");
				String destName = (String) lineageMap.get("destName");
				String destType = (String) lineageMap.get("destType");
				String action = (String) lineageMap.get("action");
				String depends = (String) lineageMap.get("depends");
				//depends gives the parent list
				if (depends != null) {
					srcName = null;
					srcType = "table";
				}
				TitanVertex src = null;
				TitanVertex dest = null;
				Logger.info("**********************got srcName= " + srcName + ", destName= " + destName + ", depends= " + depends);
				if (srcName != null) {
					src = getVertex(srcName, srcType);
					if (src == null) {
						src = graph.addVertex(srcName);
						src.property("name", srcName);
						src.property("type", srcType);
						src.property("src_entity_ref", "");
					}
				}
				if (destName != null) {
					dest = getVertex(destName, destType);
					if (dest == null) {
						dest = graph.addVertex(destName);
						dest.property("name", destName);
						dest.property("type", destType);
					}
				}

				if (src != null && dest != null)
					src.addEdge(action, dest, "name", action, "count", count);
				
				if (depends != null) {
					String[] parentList = depends.split(",");
					for (int j = 0; j < parentList.length; j++) {
						int refIndex = parentList[j].indexOf(":");
						srcName = parentList[j].substring(0, refIndex);
						String ref = "";
						if (parentList[j].length() > refIndex)
							ref = parentList[j].substring(refIndex + 1);
						src = getVertex(srcName, srcType);
						if (src == null) {
							src = graph.addVertex(srcName);
							src.property("name", srcName);
							src.property("type", srcType);
							src.property("src_entity_ref", ref);
						}
						if (src != null && dest != null)
							src.addEdge(action, dest, "name", action, "count", count);
					}
				}
			} catch (SemanticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		graph.tx().commit();
		// TitanManagement mgmt = graph.openManagement();
		// try {
		// mgmt.updateIndex(mgmt.getGraphIndex("byNameType"),
		// SchemaAction.REINDEX).get();
		// mgmt.updateIndex(mgmt.getGraphIndex("byNameAndType"),
		// SchemaAction.REINDEX).get();
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (ExecutionException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// mgmt.commit();

		Logger.info("********************got vertices count= " + IteratorUtils.count(graph.vertices()));
		Logger.info("********************got edges count= " + IteratorUtils.count(graph.edges()));

		Iterator vlist = graph.vertices();
		while (vlist.hasNext()) {
			TitanVertex vertex = (TitanVertex) vlist.next();
			Logger.info("*****************got vertex= " + IteratorUtils.asList(vertex.values()));
		}
		Iterator elist = IteratorUtils.asIterator(graph.edges());
		while (elist.hasNext()) {
			TitanEdge edge = (TitanEdge) elist.next();
			Logger.info("*******************got graph.edges=" + IteratorUtils.asList(edge.values()));
		}

		ArrayList list = new ArrayList();
		GraphTraversalSource g = graph.traversal();
		//g.E().path().by(__.valueMap(true)).fill(list);
		//g.V().path().by(__.valueMap(true)).fill(list);
		//g.V().has("name","invites").has("type", "table").path().by(__.valueMap(true)).fill(list);
		//g.V().has("name","invites").has("type", "table").bothE().otherV().path().by(__.valueMap(true)).fill(list);
		//g.V().has("name","TestUser").has("type", "user").bothE().otherV().path().by(__.valueMap(true)).fill(list);
		
		list.clear();
		g.V().has("name","TestUser").has("type", "user").bothE().otherV().path().by(__.valueMap(true)).fill(list);
		Logger.info("***************got result= " + list.toString());
		String json = new Gson().toJson(list);
		Logger.info("*********************json result= " + json);
		
		list.clear();
		g.V().has("name","invites").has("type", "table").bothE().otherV().path().by(__.valueMap(true)).unfold().fill(list);
		Logger.info("***************got result= " + list.toString());
		json = new Gson().toJson(list);
		Logger.info("*********************json result= " + json);
		
		list.clear();
		//g.V().has("name","invites").has("type", "table").as("output").select("output").path().by(__.id()).fill(list);
		//g.V().as("output").has("name","invites").has("type", "table").as("output1").select("output", "output1").by("name").fill(list);
		
//		GraphTraversal<Vertex, Vertex> getter = g.V().has("name", "TestUser").has("type", "user");
//		Optional<Vertex> vertex = getter.tryNext();
//		Logger.info("******************got vertex= "+ vertex.get());
//		Logger.info("******************got vertex= "+ g.V().has("name", "TestUser").has("type", "user").next());
		//TitanVertex fromNode = (TitanVertex) g.V().has("name", "TestUser").has("type", "user").next();
		//TitanVertex toNode = (TitanVertex) g.V().has("name", "invites").has("type", "table").next();
//        g.V(fromNode).repeat(__.both().simplePath()).until(__.is(toNode)).limit(1).path().by(__.valueMap(true)).fill(list);
		
		//g.V(fromNode).repeat(__.both()).emit().until(__.both().count().is(0)).path().by("name").fill(list);
		//g.V().has("name","TestUser").has("type", "user").repeat(__.both()).emit().bothE().otherV().path().by(__.valueMap(true)).fill(list);
		
		TitanVertex fromNode = (TitanVertex) g.V().has("name", "invites").has("type", "table").next();
		//g.V(fromNode).path().by(__.valueMap(true)).fill(list);
		g.V().has("name", "invites").has("type", "table").repeat(__.both().simplePath()).until(__.both().count().is(0)).emit().limit(10).bothE().otherV().path().by(__.valueMap(true)).unfold().fill(list);
		Logger.info("***************got result= " + list.toString());
		json = new Gson().toJson(list);
		Logger.info("*********************json result= " + json);
		//
		// GremlinPipeline pipe = new GremlinPipeline<Vertex,
		// Vertex>(g.getVertices())
		// .as("user").cast(Vertex.class);
		// Logger.info("***************count= " + pipe.count());

		// boolean flag = checkRelationExists("TestUser", "user", "customer",
		// "database", "created");
		// Logger.info("*****************************got relationCheck= " +
		// flag);

		System.exit(0);
	}

	private static TitanVertex getVertex(String vertexName, String vertexType) {
		Iterator iter = graph.query().has("name", vertexName).has("type", vertexType).vertices().iterator();
		TitanVertex vertex = null;
		while (iter.hasNext()) {
			vertex = (TitanVertex) iter.next();
			Logger.info("*******************got vertex= " + vertex.label());
			break;
		}
		return vertex;
	}
}
